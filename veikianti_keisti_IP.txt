#cloud-config
#type: master

---
hostname: coreos1
users:
  - name: core
    passwd: $1$/SUEy5QR$ZLxKfC8Y6e7PH9xGXsy5U.
    groups:
      - sudo
      - docker
      
write-files:
  - path: /etc/profile.env
    permissions: '0644'
    content: |
      export http_proxy=http://91.199.55.3:3128
      export https_proxy=http://91.199.55.3:3128 
  - path: /etc/systemd/network/10-static.network
    permissions: '0644'
    content: |
      [Match]
      Name=enp0s8
      
      [Network]
      Address=192.168.56.15

coreos:
  etcd2:
    # Generate a new token for each unique cluster from https://discovery.etcd.io/new
    # discovery: https://discovery.etcd.io/<token>
    discovery: http://discovery.etcd.io/14a1f3a159316c27aa2b9ce42cbdea6d
    discovery-proxy: http://proxy.kada.lan:8080/
    # Multi-region and multi-cloud deployments need to use $public_ipv4
    advertise-client-urls: http://192.168.56.15:2379,http://192.168.56.15:4001
    initial-advertise-peer-urls: http://192.168.56.15:2380
    
    # Listen on both the official ports and the legacy ports
    # Legacy ports can be omitted if your application doesn't depend on them
    listen-client-urls: http://0.0.0.0:2379,http://0.0.0.0:4001
    listen-peer-urls: http://192.168.56.15:2380
  
  fleet:
    public-ip: 192.168.56.15    
  
  flannel:
    interface: 192.168.56.15
  
  units:
    - name: etcd2.service
      command: start
      # See issue: https://github.com/coreos/etcd/issues/3600#issuecomment-165266437
      drop-ins:
        - name: "timeout.conf"
          content: |
            [Service]
            TimeoutStartSec=0
            
    - name: fleet.service
      command: start
      
    # Network configuration should be here, e.g:
    # - name: 00-eno1.network
    #   content: "[Match]\nName=eno1\n\n[Network]\nDHCP=yes\n\n[DHCP]\nUseMTU=9000\n"
    # - name: 00-eno2.network
    #   runtime: true
    #   content: "[Match]\nName=eno2\n\n[Network]\nDHCP=yes\n\n[DHCP]\nUseMTU=9000\n"
    
    - name: flanneld.service
      command: start
      drop-ins:
      - name: 50-network-config.conf
        content: |
          [Service]
          ExecStartPre=/usr/bin/etcdctl set /coreos.com/network/config '{ "Network": "10.1.0.0/16" }'
          
    - name: docker.service
      command: start
      drop-ins:
      - name: 60-docker-wait-for-flannel-config.conf
        content: |
          [Unit]
          After=flanneld.service
          Requires=flanneld.service

          [Service]
          Restart=always
          
    - name: docker-tcp.socket
      command: start
      enable: true
      content: |
        [Unit]
        Description=Docker Socket for the API

        [Socket]
        ListenStream=2375
        Service=docker.service
        BindIPv6Only=both

        [Install]
        WantedBy=sockets.target